# OpenML dataset: adult

https://www.openml.org/d/43880

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Predict whether income exceeds $50K/yr based on census data. Also known as Census Income dataset. Train and test sets combined. Null values represented with question mark is replaced with na. 52 duplicate values found and dropped

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43880) of an [OpenML dataset](https://www.openml.org/d/43880). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43880/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43880/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43880/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

